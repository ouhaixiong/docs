linux 分割文件（linux切割文件【split命令详解】）

bash-4.2# split --help
Usage: split [OPTION]... [INPUT [PREFIX]]
Output fixed-size pieces of INPUT to PREFIXaa, PREFIXab, ...; default
size is 1000 lines, and default PREFIX is 'x'.  With no INPUT, or when INPUT
is -, read standard input.

Mandatory arguments to long options are mandatory for short options too.
  -a, --suffix-length=N   generate suffixes of length N (default 2)
      --additional-suffix=SUFFIX  append an additional SUFFIX to file names
  -b, --bytes=SIZE        put SIZE bytes per output file
  -C, --line-bytes=SIZE   put at most SIZE bytes of lines per output file
  -d, --numeric-suffixes[=FROM]  use numeric suffixes instead of alphabetic;
                                   FROM changes the start value (default 0)
  -e, --elide-empty-files  do not generate empty output files with '-n'
      --filter=COMMAND    write to shell COMMAND; file name is $FILE
  -l, --lines=NUMBER      put NUMBER lines per output file
  -n, --number=CHUNKS     generate CHUNKS output files; see explanation below
  -u, --unbuffered        immediately copy input to output with '-n r/...'
      --verbose           print a diagnostic just before each
                            output file is opened
      --help     display this help and exit
      --version  output version information and exit

SIZE is an integer and optional unit (example: 10M is 10*1024*1024).  Units
are K, M, G, T, P, E, Z, Y (powers of 1024) or KB, MB, ... (powers of 1000).

CHUNKS may be:
N       split into N files based on size of input
K/N     output Kth of N to stdout
l/N     split into N files without splitting lines
l/K/N   output Kth of N to stdout without splitting lines
r/N     like 'l' but use round robin distribution
r/K/N   likewise but only output Kth of N to stdout

GNU coreutils online help: <http://www.gnu.org/software/coreutils/>
Report split translation bugs to <http://translationproject.org/team/>
For complete documentation, run: info coreutils 'split invocation'

用法：split [选项]... [输入 [前缀]]
将输入内容拆分为固定大小的分片并输出到"前缀aa"、"前缀ab",...；
默认以 1000 行为拆分单位，默认前缀为"x"。如果不指定文件，或
者文件为"-"，则从标准输入读取数据。

长选项必须使用的参数对于短选项时也是必需使用的。
  -a, --suffix-length=N    指定后缀长度为N (默认为2)
  -b, --bytes=大小        指定每个输出文件的字节大小
  -C, --line-bytes=大小    指定每个输出文件里最大行字节大小
  -d, --numeric-suffixes     使用数字后缀代替字母后缀
  -l, --lines=数值        指定每个输出文件有多少行
      --verbose        在每个输出文件打开前输出文件特征
      --help        显示此帮助信息并退出
      --version        显示版本信息并退出

SIZE 可以是一个可选的整数，后面跟着以下单位中的一个：
KB 1000，K 1024，MB 1000*1000，M 1024*1024，还有 G、T、P、E、Z、Y。

例如：
split -a 2 -d -b 1G access.20210824.log access.log.
-a 2  后缀是2位
-d  后缀是数字 [默认为是字母排序：如aa ab ac]
-b 1G 每个文件最大1G [ -l 10000  每10000行一个文件 ]
access.20210824.log 需要切割的文件是：access.20210824.log
access.log.     生成的子文件前缀是 "access.log."

mac的这个和linux的相比，最大的区别就是不能以数字作为后缀了。其他基本一致