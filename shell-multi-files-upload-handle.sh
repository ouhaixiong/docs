#!/bin/bash
# 批量上传附件，处理队列
urlPrefix="http://xx.xxx.com/"
routes="multi-files-upload/handle-queue"
while(true)
do
        for route in $routes
        do
                curl $urlPrefix$route 2>&1 > /dev/null
                sleep 2s
        done
        sleep 4s
done