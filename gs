tar -zxf ghostscript-9.27-linux-x86_64.tgz
cd ghostscript-9.27-linux-x86_64
cp gs-927-linux-x86_64 /usr/bin/gs

ghostscript安装：

　　yum install ghostscript

#把tmp目录下的a.pdf压缩成b.pdf  
gs -sDEVICE=pdfwrite -dPDFSETTINGS=/screen -dNOPAUSE -dBATCH -dQUIET -sOutputFile=/tmp/b.pdf  /tmp/a.pdf

# -r204x196 -g1728x2292 参数等同于 -sPAPERSIZE=a4 可不加默认
gs -q -r204x196 -g1728x2292 -sDEVICE=pdfwrite -dPDFSETTINGS=/screen -dNOPAUSE -dBATCH -dQUIET -sOutputFile=/tmp/b.pdf  /tmp/a.pdf　　

#把pdf转化为图片  图片为a1.png  a2.png a3.png ....
gs -dQUIET -dNOSAFER -dBATCH -sDEVICE=pngalpha -dNOPAUSE -dNOPROMPT -sOutputFile=/tmp/a%d.png /tmp/a.pdf



 参数说明：
-sDEVICE=pdfwrite  表示输出格式为pdf
 
-dPDFSETTINGS 

    -dPDFSETTINGS=/screen质量较低，体积较小。
    -dPDFSETTINGS=/ebook更好的质量，但略大pdf。
    -dPDFSETTINGS=/prepress输出类似于Acrobat Distiller “Prepress Optimized”设置
    -dPDFSETTINGS=/printer选择类似于Acrobat Distiller “Print Optimized”设置的输出
    -dPDFSETTINGS=/default选择用于各种用途的输出，可能会牺牲较大的输出文件

其他参数：
    "-dQUIET",    安静的意思，指代执行过程中尽可能少的输出日志等信息。（也可以简写为“-q”）
    "-dNOSAFER",    通过命令行运行
    "-dBATCH",    执行到最后一页后退出
    "-dNOPAUSE",    每一页转换之间没有停顿
    "-dNOPROMPT",    没有相关提示                       
    "-dFirstPage=1",    从第几页开始
    "-dLastPage=5",     到第几页结束  
    "-sDEVICE=pngalpha",    转换输出的文件类型装置，默认值为x11alpha
    "-g720x1280",    图片像素(-g<width>x<height>)，一般不指定，使用默认输出
    "-r300",    图片分辨率（即图片解析度为300dpi），默认值好像是72(未测试证实)
    "-sOutputFile=/opt/shanhy/error1png/%d.png",    图片输出路径，使用%d或%ld输出页数