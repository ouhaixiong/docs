dockerfile关键字

FROM:指定父镜像：指定dockerfile基于那个image构建

MAINTAINER:作者信息：用来标明这个dockerfile谁写的

LABEL：标签：用来标明dockerfile的标签，可以使用Label代替Maintainer最终都是在docker image基本信息中可以查看

RUN:执行命令：执行一段命令，默认是/bin/sh 格式：RUN command 或者RUN [“command”,”param1”,”param2”]

CMD:容器启动命令：提供启动容器时候的默认命令和ENTRYPOINT配合使用格式CMD command param1 param2 或者 [“command”,”param1”,”param2”]

ENTRYPOINT:入口：一般在制作一些执行就关闭的容器中会使用

COPY：复制文件：build的时候复制文件到image中

ADD：添加文件：build的时候添加文件到image中，不仅仅局限于当前build上下文可以来源于远程服务

ENV：环境变量：指定build时候的环境变量，可以在启动的容器的时候通过-e覆盖格式ENVname=value

ARG:构建参数：构建参数只在构建的时候使用的参数如果ENV那么ENV的相同名字的值始终覆盖的arg参数
