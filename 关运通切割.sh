#!/bin/bash
# nginx和php日记切割和清除脚本
# author:Bear
# 设置日志文件存放目录
nginx_logs_path="/home/logs/nginx/";
# 设置pid文件路径
nginx_pid_path="/home/logs/nginx/nginx.pid"

nginx_logs="56100-guanwang-access 56100-guanwang_error access baoguan100_error bc_access bc_api_error bc_error bg100_access bg100_error cas_api_error cc_access cc_api_error cds_error nginx_error";
for nginx_log in ${nginx_logs}
do
    # 重命名日志文件
    mv ${nignx_logs_path}${nginx_log}.log ${nginx_logs_path}${nginx_log}_`date -d "yesterday" +%Y%m%d`.log
done

# 向nginx主进程发信号重新打开日志
kill -USR1 `cat ${nginx_pid_path}`

for nginx_log in ${nginx_logs}
do
    # 找到15天前的带“access_*.log”的文件进行删除
    find ${nginx_logs_path} -mtime +15 -name ${nginx_log}"_*.log" -exec rm -rf {} \;
done


#php5和php7日记切割和清除
php5_logs_path="/home/logs/php5/";
php7_logs_path="/home/logs/php7/";
php5_pid_path="/home/logs/php5/pid";
php7_pid_path="/home/logs/php7/pid";

php5_logs="php-fpm.log www.access.log www.log.slow";
php7_logs="runtime_error_logsyslog www.access.log www.log.slow";

for php5_log in ${php5_logs}
do
    mv ${php5_logs_path}${php5_log} ${php5_logs_path}${php5_log}_`date -d "yesterday" +%Y%m%d`
done
kill -USR1 `cat ${php5_pid_path}`

for php7_log in ${php7_logs}
do
    mv ${php7_logs_path}${php7_log} ${php7_logs_path}${php7_log}_`date -d "yesterday" +%Y%m%d`
done
kill -USR1 `cat ${php7_pid_path}`

for php5_log in ${php5_logs}
do
    find ${php5_logs_path} -mtime +15 -name ${php5_log}"_*" -exec rm -rf {} \;
done

for php7_log in ${php7_logs}
do
    find ${php7_logs_path} -mtime +15 -name ${php7_log}"_*" -exec rm -rf {} \;
done